"""satws target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_satws.sinks import (
    SatwsSink,
)


class Targetsatws(Target):
    """Sample target for satws."""
    #  TODO: implementar los settings correctamente. Buscar la forma de pasar la url
    #   de la bd al archivo de database para traerlo desde el config, una posible
    #   solicion es usar super()

    name = "target-satws"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "database_url",
            th.StringType,
            description="The database's url for save the batch of invoices"
        ),
        th.Property(
            "aws_access_key_id",
            th.StringType,
            description="aws access key id for aws session"
        ),
        th.Property(
            "aws_secret_access_key",
            th.StringType,
            description="aws secret access key for aws session"
        ),
        th.Property(
            "region_name",
            th.StringType,
            description="Region name for bucket for aws session"
        ),
        th.Property(
            "s3_bucket",
            th.StringType,
            description="Bucket in s3 for store files"
        ),
    ).to_dict()
    default_sink_class = SatwsSink
