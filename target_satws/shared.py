import json
import re
from datetime import date, datetime
from typing import Union

import boto3


class CustomEncoder(json.JSONEncoder):
    """Json encoder for datetime objects."""

    def default(self, o: Union[date, datetime]) -> str:
        """Add custom encoders."""
        if isinstance(o, (date, datetime)):
            return o.isoformat()
        return super().default(o)


# pylint: disable=too-many-arguments
def upload_s3_object(
    file_path: str,
    data: bytes,
    bucket: str,
    aws_access_key_id: str,
    aws_secret_access_key: str,
    region_name: str,
) -> dict:
    """Upload file to S3.

    Parameters
    ----------
    file_path: str
        Path to save file in s3
    data: bytes
        Data to be stored in the file in the `file_path`
    bucket: str
        Bucket to save file in s3
    aws_access_key_id: str
        Variable for aws credential
    aws_secret_access_key: str
        Variable for aws credential
    region_name: str
        Region for aws credential

    Returns
    -------
    response: dict
    """
    s3_client = boto3.client(
        "s3",
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name,
    )

    response = s3_client.put_object(
        Bucket=bucket,
        Key=file_path,
        Body=data,
    )

    return response


def to_snake_case(word: str) -> str:
    """Make an underscored, lowercase form from the expression in the string."""
    word = re.sub(r"([A-Z]+)([A-Z][a-z])", r"\1_\2", word)
    word = re.sub(r"([a-z\d])([A-Z])", r"\1_\2", word)
    word = word.replace("-", "_")
    return word.lower()
