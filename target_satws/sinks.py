"""satws target sink class, which handles writing streams."""

import json
import uuid
from datetime import datetime
from itertools import chain
from pathlib import Path
from typing import Union

from singer_sdk.sinks import BatchSink

from target_satws.database import (
    Invoices,
    InvoicesItems,
    InvoicesRelations,
    SuppliersCompany,
    insights,
)
from target_satws.shared import CustomEncoder, to_snake_case, upload_s3_object

ModelType = Union[type(Invoices), type(InvoicesItems), type(InvoicesRelations)]


class SatwsSink(BatchSink):
    """satws target sink class."""

    max_size = 2000  # Max records to write in one batch
    batch_zise = 2000  # Batch for save in database

    # pylint: disable=too-many-statements
    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        format_date = "%Y-%m-%d %H:%M:%S"
        strptime = datetime.strptime
        record["canceledAt"] = strptime(record["canceledAt"], format_date) if record["canceledAt"] else None
        record["certifiedAt"] = strptime(record["certifiedAt"], format_date)
        record["createdAt"] = strptime(record["createdAt"], format_date)
        record["fullyPaidAt"] = strptime(record["fullyPaidAt"], format_date) if record["fullyPaidAt"] else None
        record["issuedAt"] = strptime(record["issuedAt"], format_date)
        record["lastPaymentDate"] = (
            strptime(record["lastPaymentDate"], format_date) if record["lastPaymentDate"] else None
        )
        record["updatedAt"] = strptime(record["updatedAt"], format_date)
        record["sat_ws_id"] = record["id"]
        record["supplier_id"] = None
        record["is_removed"] = False

        tax_id = record["receiver"]["rfc"] if record["isIssuer"] else record["issuer"]["rfc"]

        items = record.pop("items")
        relations = record.pop("relations")

        for item in items:
            item["id"] = str(uuid.uuid4())
            item["is_removed"] = False
            item["invoice_id"] = record["id"]
            item["created"] = record["createdAt"]
            item["modified"] = record["updatedAt"]
            item.pop("taxes", None)

        for relation in relations:
            relation["createdAt"] = strptime(relation["createdAt"], format_date) if relation["createdAt"] else None
            relation["updatedAt"] = strptime(relation["updatedAt"], format_date) if relation["updatedAt"] else None

            relation["id"] = relation["@id"].split("/")[-1]
            relation["created"] = relation.pop("createdAt")
            relation["invoice_id"] = record["id"]
            relation["is_removed"] = False
            relation["modified"] = relation.pop("updatedAt")
            relation["related_invoice"] = relation.pop("relatedInvoiceUuid")

            relation.pop("@id", None)
            relation.pop("@type", None)
            relation.pop("invoice", None)
            relation.pop("relatedInvoice", None)

        record["created"] = record.pop("createdAt")
        record["modified"] = record.pop("updatedAt")
        record["last_payment_day"] = record.pop("lastPaymentDate")

        record_renamed = {to_snake_case(k): v for k, v in record.items()}
        items_renamed = [{to_snake_case(k): v for k, v in item.items()} for item in items]

        # Merge items and relations for store in s3
        invoices_for_s3 = record.copy()
        invoices_for_s3["items"] = items_renamed
        invoices_for_s3["relations"] = relations

        if "invoices_for_s3" not in context:
            context["invoices_for_s3"] = []

        if "records" not in context:
            context["records"] = []

        if "items" not in context:
            context["items"] = []

        if "tax_id" not in context:
            context["tax_id"] = []

        if "relations" not in context:
            context["relations"] = []

        context["records"].append(record_renamed)
        context["invoices_for_s3"].append(invoices_for_s3)
        context["tax_id"].append(tax_id)
        context["items"].extend(items_renamed)
        context["relations"].extend(relations)

    # pylint: disable=too-many-locals
    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        records = context.pop("records")

        self.send_to_s3(context, records)

        items = context.pop("items")
        relations = context.pop("relations")
        tax_ids = set(context.pop("tax_id"))

        id_records = [record["id"] for record in records]
        ids_in_database = Invoices.select(Invoices.id).where(Invoices.id.in_(id_records)).tuples()

        suppliers_id = (
            SuppliersCompany.select(SuppliersCompany.id, SuppliersCompany.tax_id)
            .where(SuppliersCompany.tax_id.in_(tax_ids))
            .dicts()
        )

        updateable_ids = [str(_id) for _id in chain.from_iterable(ids_in_database)]

        update_records = [record for record in records if record["id"] in updateable_ids]
        creation_records = [record for record in records if record["id"] not in updateable_ids]

        self.logger.info(f"{len(creation_records)} invoices will be created")
        self.logger.info(f"{len(update_records)} invoices will be updated or omitted if the status is the same")

        creation_items = [item for item in items if item["invoice_id"] not in updateable_ids]
        creation_relations = [relation for relation in relations if relation["invoice_id"] not in updateable_ids]

        suppliers_tax_id = {v["tax_id"] for v in suppliers_id}

        for record in creation_records:
            tax_id = record["receiver"]["rfc"] if record["is_issuer"] else record["issuer"]["rfc"]
            if tax_id in suppliers_tax_id:
                supplier_id = [v["id"] for v in suppliers_id if v["tax_id"] == tax_id][0]
                record["supplier_id"] = supplier_id

        # Bulk update
        records_updated_models = [Invoices(**data) for data in update_records]
        with insights.atomic():
            Invoices.bulk_update(records_updated_models, fields=["status"], batch_size=self.batch_zise)

        # Bulk create - Insert Many
        # Insert invoices
        if creation_records:
            with insights.atomic():
                self.insert_many(model=Invoices, records=creation_records)
            self.logger.info(f"{len(creation_records)} invoices were created")

        # Insert items
        if creation_items:
            with insights.atomic():
                self.insert_many(model=InvoicesItems, records=creation_items)
            self.logger.info(f"{len(creation_items)} items were created")

        # Insert relations
        if creation_relations:
            with insights.atomic():
                self.insert_many(model=InvoicesRelations, records=creation_relations)
            self.logger.info(f"{len(creation_relations)} relations were created")

        # creation of non-inserted invoices
        creation_ids = [record["id"] for record in creation_records]
        creation_ids_database = Invoices.select(Invoices.id).where(Invoices.id.in_(creation_ids)).tuples()
        remaining_ids = [str(_id) for _id in chain.from_iterable(creation_ids_database)]
        remaining_creation_ids = [record for record in creation_records if record["id"] not in remaining_ids]

        if remaining_creation_ids:
            self.insert_many(model=Invoices, records=remaining_creation_ids)
            self.logger.info(f"{len(remaining_creation_ids)} remaining invoices were created")

    @staticmethod
    def insert_many(model: ModelType, records: list) -> None:
        """Insert many statement."""
        # pylint: disable=no-value-for-parameter
        model.insert_many(records).on_conflict_ignore().execute()

    def send_to_s3(self, context: dict, records: list) -> None:
        """Send data in json format to s3."""
        invoices_for_s3 = context.pop("invoices_for_s3")
        invoices = json.dumps(invoices_for_s3, cls=CustomEncoder)
        first_record = records[0]
        company_id = first_record["company_id"]
        rfc = first_record["issuer"]["rfc"] if first_record["is_issuer"] else first_record["receiver"]["rfc"]
        dates = sorted({record["issued_at"] for record in records})
        from_date = dates[0].strftime("%Y%m%d")
        to_date = dates[-1].strftime("%Y%m%d")
        filename = f"{str(uuid.uuid4()).rsplit('-', maxsplit=1)[-1]}-{from_date}-{to_date}.json"
        path = str(Path("satws", company_id, rfc, filename))
        upload_s3_object(
            file_path=path,
            data=bytes(invoices, encoding="utf-8"),
            bucket=self.config["s3_bucket"],
            aws_access_key_id=self.config["aws_access_key_id"],
            aws_secret_access_key=self.config["aws_secret_access_key"],
            region_name=self.config["region_name"],
        )
        self.logger.info(f"Sending {len(invoices_for_s3)} invoices to S3")
